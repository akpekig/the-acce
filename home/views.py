from django.shortcuts import render
from django.views import generic


def index(request):
    return render(
        request,
        "index.html",
    )

def about(request):
    return render(
        request,
        "home/about.html",
    )

def get_code(request):
    return render(
        request,
        "home/get_code.html",
    )

def access_tools(request):
    return render(
        request,
        "home/access_tools.html",
    )

def privacy_policy(request):
    return render(
        request,
        "home/privacy_policy.html",
    )

def terms_of_use(request):
    return render(
        request,
        "home/terms.html",
    )

def camus_quote(request):
    return render(
        request,
        "home/justice.html",
    )

def template_test(request):
    return render(
        request,
        "test.html",
    )
