from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("about", views.about, name="About this site"),
    path("get_code", views.get_code, name="Get one time password"),
    path("access_tools", views.access_tools, name="Access your tools"),
    path("privacy_policy", views.privacy_policy, name="Privacy policy"),
    path("terms", views.terms_of_use, name="Terms of use"),
    path("justice", views.camus_quote, name="Justice"),
    path("looksgood", views.template_test, name="Template test"),
]
