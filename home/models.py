import uuid

from django.db import models
from django_countries.fields import CountryField

class Location(models.Model):
    """Model representing real-world locations."""
    key = models.UUIDField(default=uuid.uuid4, editable=False)
    line_1 = models.CharField(max_length=128, blank=True, null=True)
    line_2 = models.CharField(max_length=128, blank=True, null=True)
    town = models.CharField(max_length=48)
    country = CountryField()
    postcode = models.CharField(max_length=24)

    def __str__(self):
        """Returns human-readable reference to model instance."""
        return self.line_1